import pytest

from white_box import count_sign_changes


@pytest.mark.parametrize(
    ('n', 'arr', 'expected'), [
        (0, [], 0),
        (3, [1, 2, 3], 0),
        (3, [1, -2, 3], 2),
    ]
)
def test_count_sign_changes_graph(n, arr, expected):
    assert count_sign_changes(n, arr) == expected


@pytest.mark.parametrize(
    ('n', 'arr', 'expected'), [
        (3, [1, 2, 3], 0),
        (0, [], 0),
        (3, [1, -2, 3], 2),
        (3, [1, 2, 3], 0)
    ]
)
def test_count_sign_changes_conditions(n, arr, expected):
    assert count_sign_changes(n, arr) == expected
