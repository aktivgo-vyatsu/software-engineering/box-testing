def count_sign_changes(n, arr):
    count = 0

    for i in range(1, n):
        if arr[i] * arr[i - 1] < 0:
            count += 1

    return count


if __name__ == '__main__':
    n = int(input('Input n: '))
    arr = list(map(int, input('input arr: ').split()))

    print(count_sign_changes(n, arr))
