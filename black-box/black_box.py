import math


def print_table(d: dict):
    print('x', 'result', sep='\t\t')
    for k, v in d.items():
        print(k, v, sep='\t\t')


def compute(x_min, x_max, dx, a, b) -> dict:
    result = {}

    if x_min >= x_max: return result
    if dx <= 0: return result

    for x in range(x_min, x_max + 1, dx):
        result[x] = y(x, a, b)

    return result


def y(x, a, b):
    denominator = math.pow(math.cos(x), 2) - b

    if denominator == 0 or b < 0:
        return math.nan

    return math.sin((a + math.pow(math.cos(x), 2)) / denominator) + 2.5 * a * math.sqrt(b)


if __name__ == '__main__':
    x_min = int(input("Input x_min: "))
    x_max = int(input("Input x_max: "))
    dx = int(input("Input dx: "))

    a = int(input("Input a: "))
    b = int(input("Input b: "))

    result = compute(x_min, x_max, dx, a, b)

    print_table(result)
